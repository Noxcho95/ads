import React from 'react';
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import "./index.css"


function App() {
  return (
      <div>
        <Header/>
        <Footer/>
      </div>
  );
}

export default App;
