import React from 'react';


export default function Header(props) {
    return (
        <>
            <div className="header-section">
                <div className="container-fluid">
                    <div className="logo">
                        <a href="./index.html"><img src="img/logo.png" alt=""/></a>
                    </div>
                    <nav className="main-menu mobile-menu">
                        <ul>
                            <li className="active"><a href="/">Home</a></li>
                            <li><a href="/">Explore</a></li>
                            <li><a href="/">More Cities</a></li>
                            <li><a href="/">News</a></li>
                            <li><a href="/">Contact</a></li>
                        </ul>
                    </nav>
                    <div className="header-right">
                        <div className="user-access">
                            <a href="/">Register/</a>
                            <a href="/">Login</a>
                        </div>
                        <a href="/" className="primary-btn">Add Listing</a>
                    </div>
                    <div id="mobile-menu-wrap"></div>
                </div>
            </div>

            <section className="hero-section set-bg" data-setbg="img/hero-bg.jpg">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="hero-text">
                                <img src="img/placeholder.png" alt="" />
                                    <h1>New York</h1>
                                    <form action="#" className="filter-search">
                                        <div className="category-search">
                                            <h5>Search Category</h5>
                                            <select className="ca-search">
                                                <option>Restaurants</option>
                                                <option>Hotels</option>
                                                <option>Food & Drinks</option>
                                                <option>Home Delievery</option>
                                                <option>Commercial Shops</option>
                                            </select>
                                        </div>
                                        <div className="location-search">
                                            <h5>Your Location</h5>
                                            <select className="lo-search">
                                                <option>New York</option>
                                            </select>
                                        </div>
                                        <button type="submit">Search Now</button>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
);
}

