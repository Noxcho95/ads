import React from 'react';

export default function Footer(props) {
    return (
        <div className="footer-section spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <form action="#" className="newslatter-form">
                            <input type="text" placeholder="Your email address"/>
                            <button type="submit">Subscribe to our Newsletter</button>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-widget">
                            <h4>Usefull Links</h4>
                            <ul>
                                <li>About us</li>
                                <li>Testimonials</li>
                                <li>How it works</li>
                                <li>Create an account</li>
                                <li>Our Services</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-widget">
                            <h4>Categories</h4>
                            <ul>
                                <li>Hotels</li>
                                <li>Restaurant</li>
                                <li>Spa & resorts</li>
                                <li>Concert & Events</li>
                                <li>Bars & Pubs</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-widget">
                            <h4>Usefull Links</h4>
                            <ul>
                                <li>About us</li>
                                <li>Testimonials</li>
                                <li>How it works</li>
                                <li>Create an account</li>
                                <li>Our Services</li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6">
                        <div className="footer-widget">
                            <h4>From the Blog</h4>
                            <div className="single-blog">
                                <div className="blog-pic">
                                    <img src="%PUBLIC_URL%/img/f-blog-1.jpg" alt="" />
                                </div>
                                <div className="blog-text">
                                    <h6>10 Best clubs in town</h6>
                                    <span>March 27, 2019</span>
                                </div>
                            </div>
                            <div className="single-blog">
                                <div className="blog-pic">
                                    <img src="public/img/f-blog-2.jpg" alt="" />
                                </div>
                                <div className="blog-text">
                                    <h6>10 Best clubs in town</h6>
                                    <span>March 27, 2019</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row footer-bottom">
                    <div className="col-lg-5 order-2 order-lg-1">
                        <div className="copyright">
                            <p className="text-white">

                            </p>
                        </div>
                    </div>
                    <div className="col-lg-7 text-center text-lg-right order-1 order-lg-2">
                        <div className="footer-menu">
                            <a href="#">Home</a>
                            <a href="#">Explore</a>
                            <a href="#">More Cities</a>
                            <a href="#">News</a>
                            <a href="#">Contact</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
);
}